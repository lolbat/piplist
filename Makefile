FILE=./piplist/__main__.py

run:
	textual run $(FILE)

dev:
	textual run --dev $(FILE)

console:
	textual console -x SYSTEM -x EVENT

active:
	source .venv/bin/activate

