#
# File: pipreader.py
# Author: Zac Belado
#
# Created: May 13, 2023
#
# Description: A class to read the list of pip packages and process them
#

import sh
import re
import logging
from typing import Dict, List
from pathlib import Path
from dataclasses import dataclass

from textual.logging import TextualHandler

from rich import box
from rich.panel import Panel
from rich.text import Text

logging.basicConfig(
    level="NOTSET",
    handlers=[TextualHandler()],
)


@dataclass
class pipVersion:
    version: str = ""
    location: Path = Path.cwd()
    pythonVersion: str = ""


class PipReader:

    # A class file to retreive the list of pip packages and
    # to then process them for the main app to use

    def __init__(self) -> None:

        self.packages: Dict = {"venv": {}, "global": {}}  # all of the pip packages
        self.packageData: Dict = {}  # the last package loaded
        self.pipInstalled = False
        self.pipData = pipVersion()

    def loadPipPackageData(self, packageName: str, binaryPath: Path) -> Panel:
        """
        Called when the user has clicked on a package name

        Args:
            packageName (str): the name of the pip package that we want to view
            binaryPath (Path): the path to the version of pip that we want to run
        """

        # make a pip show call
        commandLine = sh.Command(f"{binaryPath}/bin/pip")

        try:
            packageData = commandLine("show", str(packageName))
        except sh.ErrorReturnCode as e:
            print(f'Command {e.full_cmd} exited with {e.exit_code}')
            raise ValueError(f"Error reading pip package data: {e.exit_code} {e.full_cmd}")

        # split the data into lines
        data = packageData.split("\n")

        # turn it into a pretty panel to display

        packageText = Text()

        for aLine in data:

            if aLine != "":
                # we can't use split as the data isn't consistent and
                # who knows what is in it. Data is formatted as so
                # Name: data so we look for the first :
                index = aLine.index(":")
                packageAttr = aLine[:index]
                attrData = aLine[index+2:]

                packageText.append(f"{packageAttr}: ", style="Bold dodger_blue2")
                packageText.append(f"{attrData}", style="Plain white")
                packageText.append("\n\n")

        # now put all of the text into a panel
        titleStr = Text("Package Details", "Bold red1")
        returnPanel = Panel(packageText, box=box.ROUNDED, title=titleStr)

        return returnPanel

    def loadPipData(self, binarypath: Path) -> List[str]:
        """
            load the pip package data using the path provided

        Args:
            binarypath (Path.cwd, optional): The path to the pip binary

        Raises:
            ValueError: Raise a value error when trying to read the pip version
            ValueError: Raise a value error when the data is missing
        """

        # get the data for this version of pip
        # we'll store this but also use it for error checking
        commandLine = sh.Command(f"{binarypath}/bin/pip")
        versionResult = ""

        try:
            versionResult = commandLine("--version")
        except sh.ErrorReturnCode as e:
            print(f'Command {e.full_cmd} exited with {e.exit_code}')
            raise ValueError(f"Error reading pip info: {e.exit_code}")

        data = versionResult.split(" ")
        self.pipData = pipVersion()

        if data[0] == "pip":

            # it is installed so lets get some data about it
            self.pipInstalled = True
            self.pipData.version = data[1]
            self.pipData.location = Path(data[3])
            self.pipData.pythonVersion = data[5]

            # lets get the packages
            try:
                pipPackageInfo = commandLine("list")
            except sh.ErrorReturnCode as e:
                print(f'Command {e.full_cmd} exited with {e.exit_code}')
                raise ValueError(f"Error reading pip packages: {e.exit_code}")

            # split the data into lines
            pipData = pipPackageInfo.split("\n")

            # this gives us a series of lines that have a package and
            # then a series of spaces and then a version. We need to use a
            # regex to strip them out
            pattern = re.compile('( {1,})')

            theReturn = []

            for aLine in pipData:
                tabLine = (re.sub(pattern, "\t", aLine))
                data = tabLine.split("\t")
                
                if len(data) == 2:
                    if "-" not in data[0] and data[0] != "Package":
                        theReturn.append(data[0])
            
            return theReturn
        
        else:
            # well that isn't right so lets raise an error and see who salutes
            raise ValueError(f"Pip command not found at {binarypath} {data}")
