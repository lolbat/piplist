#
# packages.py
# Sunday May 28, 2023
#
# A few classes used across the app by various other classes
#
# Author: Zac Belado - pixelgeek@gmail.com
# version 0.1.0
#

from enum import Enum
from dataclasses import dataclass
from pathlib import Path


class packageType(Enum):
    # used to reference the type of pip package we are looking at
    GLOBAL = 0
    VENV = 1


@dataclass
class pipPackage:
    version: str
    name: str
    summary: str
    url: str
    author: str
    email: str
    license: str
    location: Path
    requires: str
    requiredBy: str
