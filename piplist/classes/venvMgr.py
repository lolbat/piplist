#
# venvMgr.py
# Tuesday May 23, 2023
#
# A class file to determine if the app is running in a venv 
# and if so what the path to it is
#
# Author: Zac Belado - pixelgeek@gmail.com
# version 0.1.0
#

import sys
import os
from pathlib import Path


class VenvMgr:

    isInVenv: bool = False
    venvPath: Path
    globalPath: Path

    """
        A class to store details about any venv running as well
        as where the global python binary is located
    """

    def __init__(self) -> None:
        self.loadVenvInfo()
        
    def loadVenvInfo(self):
        # lets take a look into sys and os and see if 
        # we can determine if there is an active venv

        pathTest = (sys.base_prefix != sys.prefix)
        environTest = 'VIRTUAL_ENV' in os.environ

        # the global python path should always be sys.base_prefix
        self.globalPath = Path(sys.base_prefix)

        if pathTest or environTest:
            # lets store the fact that we are in a venv
            self.isInVenv = True
            self.venvPath = Path(sys.prefix)
        else:
            self.isInVenv = False
            self.venvPath = self.globalPath

    def getVenvStatus(self) -> bool:
        return self.isInVenv
    
    def getVenvPath(self) -> Path:
        return self.venvPath
    
    def getPythonPath(self) -> Path:
        return self.globalPath
    
    def refreshVenvData(self) -> None:
        self.loadVenvInfo()
