#
# piplist.py
# Friday May 12, 2023
#
# A Textual app to display some data from pip
#
# Author: Zac Belado - pixelgeek@gmail.com
# version 0.1.0
#

import logging
from typing import List

from textual import on
from textual.app import App, ComposeResult
from textual.widgets import Footer, Header, TextLog, Tree, LoadingIndicator
from textual.containers import Horizontal
from textual.widgets.tree import TreeNode
from textual.logging import TextualHandler

from rich.panel import Panel

from classes.pipreader import PipReader
from classes.venvMgr import VenvMgr
from classes.packages import packageType

logging.basicConfig(
    level="NOTSET",
    handlers=[TextualHandler()],
)

pipReader = PipReader()
venvData = VenvMgr()

class pipList(App):
    """
        A Textual app to read and display data from Pip
        It will look for a venv when it runs and if it can find one
        it will load the venv as well as the global pip packages
    """

    CSS_PATH = "piplist.css"
    BINDINGS = [("q", "quit", "Quit"), ("r", "refresh", "Refresh"), ("m", "toggleMode", "Mode")]

    def showStatus(self, newStatus: str) -> None:
        status = self.query_one("#status", TextLog)
        status.write(f"Status: {newStatus}")

    def compose(self) -> ComposeResult:

        yield Header(name="pipList")

        yield TextLog(id="status")

        with Horizontal(id="displayContainer"):
            yield LoadingIndicator()
            yield Tree("Pip Packages", id="packageList")
            yield TextLog(id="display")

        yield Footer()

    def action_toggleMode(self) -> None:
        self.dark = not self.dark

    def loadPackageData(self, pipType: packageType) -> List[str]:
        """
        Call the pipReader to get the appropriate list of pip packages

        Args:
            pipType (packageType): Either .VENV or .GLOBAL.

        Returns:
            List[str]: a list of package names from the pipReader class
        """

        # call to the pip reader and get it to list the packages in pip
        if pipType == packageType.GLOBAL:
            return pipReader.loadPipData(venvData.getPythonPath())
        else:
            # check to see if there is an active venv
            if venvData.getVenvStatus():
                # there is one so ask for the data from it
                return pipReader.loadPipData(venvData.getVenvPath())

    def on_mount(self) -> None:

        self.loadPipData()

    def loadPipData(self) -> None:
  
        # load the pip data and display it
        if venvData.getVenvStatus():
            packageData = self.loadPackageData(packageType.VENV)
            self.displayPackageData(packageData, packageType.VENV)

        packageData = self.loadPackageData(packageType.GLOBAL)
        self.displayPackageData(packageData, packageType.GLOBAL)

        self.query_one(LoadingIndicator).display = False

    def displayPackageData(self, packages: List[str], pipType: packageType) -> None:
        """ Display a list of pip packages in the Tree

        Args:
            packages (List[str]): a list of the package names
            pipType (packageType): enum type for the pip data type
        """        

        # check the package data and if it is not empty then add a node for it

        nodeName = "global packages"
        if pipType == packageType.VENV:
            nodeName = "venv packages"

        if len(packages):
            pipTree = self.query_one(Tree)
            currentNode = pipTree.root.add(nodeName, expand=True)

            for aPackage in packages:
                currentNode.add(aPackage, expand=False, allow_expand=False)

    @on(Tree.NodeSelected, "#packageList")
    def loadPackage(self, node: TreeNode) -> None:
        """
        React to the event sent by the Tree when it is clicked on
            
        Args:
            node: TreeNode - the node that was selected

        """
        packageNode = node.node  # this seems a bit odd but it is how it works

        # get the label and then use that to pull data about the pip package
        nodeLabel = packageNode.label
        nodeParent = packageNode.parent
        parentLabel = nodeParent.label

        binaryPath = venvData.globalPath

        if str(parentLabel) == "venv packages":
            binaryPath = venvData.venvPath

        displayArea = self.query_one("#display", TextLog)
        displayArea.clear()
        displayArea.write(f"Loading pip data for {nodeLabel}")

        displayData: Panel = pipReader.loadPipPackageData(nodeLabel, binaryPath)

        # then display the nice table
        displayArea.clear()
        displayArea.write(displayData)


if __name__ == "__main__":
    pipList().run()
