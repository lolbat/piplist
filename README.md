# piplist

![pipList screen](images/screen.jpg)

pipList is a command line application built using [Textual](https://github.com/textualize/textual/). This is a very early version of the project and currently the app doesn't do much more than list the Python packages in your global Python installation as well as any virtual environments that are defined in your terminal.

## Usage

The project has been built with [poetry](https://python-poetry.org/docs/) so you should be able to install it using that. If not, pipList requires Textual 0.24.1 and Rich 13.3.5.

You can run the app from the root directory by using

`python piplist`

If you want to run it using Textual it needs to be run with a direct path to the app file

`textual run piplist/__main__.py`

There is a short delay when running, I need to add a loading screen, but you should see a list of the global pip packages as well as the packages in any venv in the working directory. 

Click on a package and it will load the appropriate data and display it.

## Plans

1) Adding a loading screen
2) Storing the various package locations in a list
3) Allowing you to add other vents from different locations
4) Building a tool to diff between two sets of pip packages